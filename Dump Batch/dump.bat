:: This software is licensed under the GNU General Public License version 3.0 (GPL-3.0).
:: Notice:
:: - The original work is freely available from https://gitlab.com/shinthebean/batchfiles                               
:: - Any modifications made to this software must be licensed under GPL v3.0.
:: - ABSOLUTELY NO WARRANTIES ARE PROVIDED. THE SOFTWARE IS PROVIDED AS IS AND THE USER ASSUMES ALL RISK THAT MAY BE CONNECTED WITH USING IT AS WELL AS RESPONSIBILITY FOR ALL WOULD BE DAMAGES.
:: For the complete terms and conditions, please refer to the official GNU GPL v3.0 license text available at:
:: https://www.gnu.org/licenses/gpl-3.0.en.html or in the source repository

@echo off
title Minidump Folder Converter
echo Checking if there are any dump files..
set dump_folder=%systemroot%\minidump
dir /b "%dump_folder%\*.dmp" > nul 2>&1
if errorlevel 1 (
    echo No .dmp files found in %dump_folder%.
	echo Closing in a few seconds..
	timeout 5
	exit /b
) else (
    echo .dmp files found in %dump_folder%.
goto filecheck
)
:filecheck
echo Checking for already made files..
rd /s /q %USERPROFILE%\Desktop\minidumps 2>nul
rd /s /q %systemroot%\minidump\files 2>nul

echo Duplicate files deleted.
echo.

mkdir "%systemroot%\minidump\files"
echo Created \files in minidump folder.
mkdir "%USERPROFILE%\Desktop\minidumps"
echo Created minidumps folder on desktop.
timeout 1 > nul
MOVE "%systemroot%\minidump\*.*" "%systemroot%\minidump\files" 2>nul
MOVE "%systemroot%\minidump\files\*.*" "%USERPROFILE%\Desktop\minidumps" 2>nul
timeout 1 > nul
setlocal enabledelayedexpansion
set folder="%USERPROFILE%\Desktop\minidumps"
set zip="%USERPROFILE%\Desktop\minidumps.zip"
if exist %zip% del %zip%
pushd %folder%
powershell Compress-Archive -Path . -DestinationPath %zip%
popd
MOVE "%USERPROFILE%\Desktop\minidumps.zip\minidumps\*.*" "%USERPROFILE%\Desktop\minidumps.zip\" 2>nul
echo.
echo --------------------------
echo  FILES READY TO BE SHARED
echo --------------------------
echo.
choice /c YN /m "Do you want to delete the excess folders?"
if errorlevel 2 goto NoDelete

echo Deleting excess files in a few seconds..
timeout 2 > nul

rd /s /q %USERPROFILE%\Desktop\minidumps 2>nul
rd /s /q %systemroot%\minidump\files 2>nul
echo.
echo -------------------------
echo    EXCESS FILES DELETED
echo -------------------------
echo.
goto EndScript

:EndScript
echo Now deleting batch file..
timeout 3 > nul
del "%~f0"

:NoDelete
echo.
echo You have chosen to not delete the excess files; Meaning any files created from this batch file you will have to delete yourself.
echo Exiting..
timeout 7 > nul
exit