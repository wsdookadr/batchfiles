:: This software is licensed under the GNU General Public License version 3.0 (GPL-3.0).
:: Notice:
:: - The original work is freely available from https://gitlab.com/shinthebean/batchfiles                               
:: - Any modifications made to this software must be licensed under GPL v3.0.
:: - ABSOLUTELY NO WARRANTIES ARE PROVIDED. THE SOFTWARE IS PROVIDED AS IS AND THE USER ASSUMES ALL RISK THAT MAY BE CONNECTED WITH USING IT AS WELL AS RESPONSIBILITY FOR ALL WOULD BE DAMAGES.
:: For the complete terms and conditions, please refer to the official GNU GPL v3.0 license text available at:
:: https://www.gnu.org/licenses/gpl-3.0.en.html or in the source repository

@echo off
echo                           Created by shinthebean
echo                    for PC Help Hub Discord and General Use
echo                   https://gitlab.com/shinthebean/batchfiles
echo.
echo.


for /f "usebackq tokens=*" %%A in (`powershell -command "hostname"`) do (
    set "COMPUTER_NAME=%%A"
)

echo ----------------------------------------
echo     GATHERING SPECS from %COMPUTER_NAME%
echo -----------------------------------------
echo.

:: CPU
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_Processor).Name"`) do (
    set "CPU=%%A"
)

:: GPU
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_VideoController).Caption"`) do (
    set "GPU=%%A"
)

:: RAM Total
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum).Sum / 1GB"`) do (
    set "TOTAL_PHYSICAL_MEMORY=%%A GB"
)

:: Motherboard
for /F "usebackq tokens=*" %%A in (`powershell -command "Get-WmiObject Win32_BaseBoard | Select-Object -ExpandProperty Product"`) do (
    set "MOTHERBOARD=%%A"
)

:: BIOS Type
for /f "usebackq tokens=*" %%A in (`powershell -command "Get-WmiObject Win32_ComputerSystem | ForEach-Object { if ($_.BootMode -eq 1) { 'Legacy' } else { 'UEFI' }}"`) do (
	set "BIOSMode=%%A"
)

:: BIOS Version
for /f "usebackq tokens=*" %%A in (`powershell -command "Get-WmiObject Win32_BIOS | Select-Object -ExpandProperty SMBIOSBIOSVersion"`) do (
    set "BIOS=%%A"
)

:: BIOS Date
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-WmiObject Win32_BIOS).ReleaseDate | ForEach-Object { [datetime]::ParseExact($_.Substring(0,8), 'yyyyMMdd', $null).ToString('MM/dd/yyyy') }"`) do (
    set "BIOS_DATE=%%A"
)

:: OS Version
for /f "usebackq tokens=*" %%A in (`powershell -command "(Get-CimInstance Win32_OperatingSystem).Caption"`) do (
    set "OS_VERSION=%%A"
)

:: Secure Boot
for /f "usebackq tokens=*" %%A in (`powershell -command "if ((Get-ItemProperty -Path 'HKLM:\\SYSTEM\\CurrentControlSet\\Control\\SecureBoot\\State' -Name UEFISecureBootEnabled).UEFISecureBootEnabled -eq 1) { 'Enabled' } else { 'Disabled' }"`) do ( :: Checks registry for secureboot state, if value = 0 it shows as diabled; vise versa.
    set "SECURE_BOOT=%%A"
)

:: Output to Terminal
echo Computer Name: %COMPUTER_NAME%
echo CPU: %CPU%
echo GPU: %GPU%
echo RAM Installed: %TOTAL_PHYSICAL_MEMORY%
echo.
echo Motherboard: %MOTHERBOARD%
echo BIOS Version: %BIOS% %BIOS_DATE%
echo BIOS Mode: %BIOSMode%
echo.
echo OS Version: %OS_VERSION%
echo Secure Boot: %SECURE_BOOT%
echo.
echo Moving Output file to Desktop..

:OutputFile
:: Output to File (specs_list.txt)
(
    echo Computer Name: %COMPUTER_NAME%
    echo CPU: %CPU%
    echo GPU: %GPU%
	echo RAM Installed: %TOTAL_PHYSICAL_MEMORY%
    echo Motherboard: %MOTHERBOARD%
    echo BIOS Version: %BIOS% %BIOS_DATE%
	echo BIOS Mode: %BIOSMode%
    echo OS Version: %OS_VERSION%
	echo Secure Boot: %SECURE_BOOT%
) > specs_list.txt

:: Define variables
set "filename=specs_list.txt"
set "desktopfolder=%USERPROFILE%\Desktop"

:: Search for output.txt; move to desktop
powershell -command "Get-ChildItem -Path C:\ -Filter %filename% -Recurse -ErrorAction SilentlyContinue | ForEach-Object { Move-Item -Path $_.FullName -Destination '%desktopfolder%' -Force }"
echo specs_list.txt moved to your Desktop..
echo If file not found on desktop, rerun the batch file.


pause
timeout 5 > nul
exit
