:: This software is licensed under the GNU General Public License version 3.0 (GPL-3.0).
:: Notice:
:: - The original work is freely available from https://gitlab.com/shinthebean/batchfiles                               
:: - Any modifications made to this software must be licensed under GPL v3.0.
:: - ABSOLUTELY NO WARRANTIES ARE PROVIDED. THE SOFTWARE IS PROVIDED AS IS AND THE USER ASSUMES ALL RISK THAT MAY BE CONNECTED WITH USING IT AS WELL AS RESPONSIBILITY FOR ALL WOULD BE DAMAGES.
:: For the complete terms and conditions, please refer to the official GNU GPL v3.0 license text available at:
:: https://www.gnu.org/licenses/gpl-3.0.en.html or in the source repository
:: UAC Check from https://superuser.com/questions/694761/how-to-run-cmd-with-admin-privileges-using-command-line/1230815#1230815

@echo off
title: System File Scan

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else (
    goto gotAdmin
)

:UACPrompt
echo Set UAC = CreateObject("Shell.Application") > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
"%temp%\getadmin.vbs"
cls
echo Command Prompt is required to be an Administrator for this to run.
echo Closing in 3 seconds.
timeout 3
exit /B

:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
pushd "%CD%"
CD /D "%~dp0"

echo ----------------------------------
echo    STARTING SYSTEM FILE CHECKER
echo ----------------------------------
echo.
echo.
echo sfc /scannow
sfc /scannow
echo.
echo.
cls
echo ----------------------------------
echo    SYSTEM FILE CHECKER FINISHED
echo ----------------------------------
echo.
echo.
echo It is recommended to restart your computer after running SFC (System File Checker) to ensure that the new files are placed properly.
choice /c YN /m "Do you want to Restart your Computer?"
if errorlevel 2 goto NoRestart
goto RestartPC

:RestartPC
echo Restarting PC..
timeout 3 > nul
shutdown /r /f /t 0
exit

:NoRestart
echo Exiting..
timeout /t 2 > nul
exit