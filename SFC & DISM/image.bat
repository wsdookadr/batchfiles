:: This software is licensed under the GNU General Public License version 3.0 (GPL-3.0).
:: Notice:
:: - The original work is freely available from https://gitlab.com/shinthebean/batchfiles                               
:: - Any modifications made to this software must be licensed under GPL v3.0.
:: - ABSOLUTELY NO WARRANTIES ARE PROVIDED. THE SOFTWARE IS PROVIDED AS IS AND THE USER ASSUMES ALL RISK THAT MAY BE CONNECTED WITH USING IT AS WELL AS RESPONSIBILITY FOR ALL WOULD BE DAMAGES.
:: For the complete terms and conditions, please refer to the official GNU GPL v3.0 license text available at:
:: https://www.gnu.org/licenses/gpl-3.0.en.html or in the source repository
:: UAC Check from https://superuser.com/questions/694761/how-to-run-cmd-with-admin-privileges-using-command-line/1230815#1230815

@echo off
title: DISM

>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else (
    goto gotAdmin
)

:UACPrompt
echo Set UAC = CreateObject("Shell.Application") > "%temp%\getadmin.vbs"
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"
"%temp%\getadmin.vbs"
cls
echo Command Prompt is required to be an Administrator for this to run.
echo Closing in 3 seconds.
timeout 3 > nul
exit /B

:gotAdmin
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
pushd "%CD%"
CD /D "%~dp0"

:: Starting commands
echo                           Created by shinthebean
echo                           for PC Help Hub Discord
echo                    With credits of inspiration to: jheden
echo                   https://gitlab.com/shinthebean/batchfiles
echo.
echo.
echo -------------------------------------------
echo       STARTING DISM COMMANDS
echo -------------------------------------------
echo.
echo DISM /Online /Cleanup-Image /startcomponentcleanup
DISM /Online /Cleanup-Image /startcomponentcleanup
timeout 2 > nul
cls
echo DISM /Online /Cleanup-Image /CheckHealth
DISM /Online /Cleanup-Image /CheckHealth
timeout 2 > nul
cls
echo DISM /Online /Cleanup-Image /ScanHealth
DISM /Online /Cleanup-Image /ScanHealth
timeout 2 > nul
cls
echo DISM /Online /Cleanup-Image /RestoreHealth
DISM /Online /Cleanup-Image /RestoreHealth
timeout 3 > nul
cls
:NotSaved2
echo -----------------------------------------
echo         DISM COMMANDS FINISHED
echo -----------------------------------------
echo.
echo To complete this operation, your PC will restart and then you will need to run another batch file..

choice /c YN /m "Have you closed your applications and saved your work? Please make sure to do so before this restart!"
if errorlevel 2 goto NotSaved
goto CheckSFCFile

:NotSaved
cls
goto NotSaved2

:CheckSFCFile
if exist "%~dp0sfc2.bat" (
move "%~dp0sfc2.bat" "%USERPROFILE%\Desktop"
    goto RestartPC
) else (
	cls
    echo SFC2.bat not detected on your system; Go back to the instructions you were given and download the SFC2.bat; Once downloaded, press Y to continue with the script.
    goto NotSaved
)

:RestartPC
pause
echo PC is Restarting..
timeout 2 > nul
shutdown /r /f /t 0